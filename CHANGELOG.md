# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 2019-01-18

### New features

- [New option](https://www.npmjs.com/package/junit-xml#schema-default-ant-junit) to have the output conform to the Azure DevOps expected JUnit format.

## [1.0.0] - 2019-01-18

### New features

- First release!
