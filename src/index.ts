import xml from 'xml';
import { TestSuiteReport } from './TestResults';
import { getXmlObject as getDefaultXmlObject } from './default';
import { getXmlObject as getAntJunitXmlObject } from './ant-junit';

export { TestSuiteReport, TestSuite, TestCase } from './TestResults';

interface Options {
  /**
   * Whether to conform to a specific schema. Possible options:
   * - `default` The output is usable by GitLab.
   * - `ant-junit`: Used by Azure DevOps. Found here: https://github.com/windyroad/JUnit-Schema/blob/master/JUnit.xsd
   */
  schema: 'default' | 'ant-junit'
};
export function getJunitXml(report: TestSuiteReport, options: Partial<Options> = {}): string {
  let xmlObject;
  switch(options.schema) {
    case 'ant-junit':
      xmlObject = getAntJunitXmlObject(report);
      break;
    default:
      xmlObject = getDefaultXmlObject(report);
  }

  return xml(xmlObject);
}
