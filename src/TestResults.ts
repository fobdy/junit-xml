export interface TestSuiteReport {
  name?: string;
  time?: number;
  suites: TestSuite[];
};

export interface TestSuite {
  testCases: TestCase[];
  name?: string;
  timestamp?: Date;
  time?: number;
  hostname?: string;
};

export interface TestCase {
  name: string;
  assertions?: number;
  skipped?: boolean;
  errors?: Array<{ message: string; type?: string; }>;
  failures?: Array<{ message: string; type?: string; }>;
  systemOut?: string[];
  systemErr?: string[];
  time?: number;
  classname?: string;
};
